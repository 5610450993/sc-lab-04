package model;

public class StarModel {

	public String style(String style,int size) 
	{
		String star = "";
		if (style.equals("Style 1"))
		{
			for (int i=1;i<=size;i++)
			{
				for (int j=1;j<=size+1;j++)
				{
					star += "*";
				}
				star += "\n";
			}
			return star;
		}
		else if (style.equals("Style 2")) 
		{
			for (int i=1;i<=size;i++)
			{
				for (int j=1;j<=size-1;j++)
				{
					star += "*";
				}
				star += "\n";
			}
			return star;
		}
		else if (style.equals("Style 3")) 
		{
			for (int i=1;i<=size;i++)
			{
				for (int j=1;j<=i;j++)
				{
					star += "*";
				}
				star += "\n";
			}
			return star;
		}
		else if (style.equals("Style 4")) 
		{
			for (int i=1;i<=size;i++)
			{
				for (int j=1;j<=size+2;j++)
				{
					if (j%2==0){star += "*";}
					else {star += "_";}
				}
				star += "\n";
			}
			return star;
		}
		else 
		{
			for (int i=1;i<=size;i++)
			{
				for (int j=1;j<=size+2;j++)
				{
					if ((i+j)%2==0){star += "*";}
					else {star += " ";}
				}
				star += "\n";
			}
			return star;
		}
	}
}
