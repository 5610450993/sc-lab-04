package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.StarModel;
import view.StarView;

public class StarController {
	private StarModel model ;
	private StarView view ;
	private ActionListener actionListener1 ;
	private ActionListener actionListener2 ;
	private String t = "THE STAR\n----------------------------\n";
	
	public StarController(StarModel model,StarView view){
		this.model = model;
		this.view = view;
	}
	
	public void starListener()
	{
		actionListener1 = new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				String style = view.getItem();
				int size = Integer.parseInt(view.getTextField().getText());
				model.style(style,size);
				t += model.style(style,size);
				t += "\n";
				view.getTextArea().setText(t);
			}
		};
		view.getButton1().addActionListener(actionListener1); 
		
		actionListener2 = new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				reset();
				view.getTextArea().setText(t);
			}
		};
		view.getButton2().addActionListener(actionListener2); 
	}
		
	public void reset(){
		this.t = "THE STAR\n----------------------------\n";
	}
}
