package controller;

import model.StarModel;
import view.StarView;


public class MVCStar {
	public static void main(String[] args)
	{
		StarModel m = new StarModel();
		StarView v = new StarView();
		StarController c = new StarController(m, v);
		c.starListener();
	}
}
