package view;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class StarView extends JFrame{
	private JTextArea textarea ;
	private JComboBox<String> box ;
	private JLabel label ;
	private JTextField textfield ;
	private JButton button1 ;
	private JButton button2 ;
	
	public StarView ()
	{
		setTitle("Star");
	    setSize(600, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null);
		
		textarea = new JTextArea("THE STAR\n----------------------------\n");
		textarea.setBounds(8,8,300,254);
		
		String[] style = new String[] {"Style 1", "Style 2", "Style 3", "Style 4", "Style 5"};
		box = new JComboBox<>(style);
		box.setBounds(320,8,250,30);
		
		label = new JLabel("Size  :");
		label.setBounds(320,68,50,30);
		
		textfield = new JTextField();
		textfield.setBounds(360,68,210,30);
		
		button1 = new JButton("show the result");
		button1.setBounds(320,224,250,30);
		
		button2 = new JButton("reset");
		button2.setBounds(320,184,250,30);
		
		add(textarea);
		add(box);
		add(label);
		add(textfield);
		add(button1);
		add(button2);
		
		setVisible(true);
	    setResizable(false);
	}
	
	public String getItem()
	{
		String selectedstyle = (String) box.getSelectedItem();
		return selectedstyle;
	}
	
	public JTextField getTextField()
	{
		return textfield;
	}
	
	public JButton getButton1()
	{
		return button1;
	}
	
	public JButton getButton2()
	{
		return button2;
	}
	
	public JTextArea getTextArea()
	{
		return textarea;
	}
}
